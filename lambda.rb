

div_by = 2

filter_div_by = ->(set) do
  set.select { |item| item % div_by == 0 }
end

def filtruj(filter)
  numbers = [1,2,3,4,5,6,7,8,9]
  filtered_numbers = filter.call(numbers)
  puts
  puts filtered_numbers
end

filtruj(filter_div_by)

div_by = 3

filtruj(filter_div_by)
